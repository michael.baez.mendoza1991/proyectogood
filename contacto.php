<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CDN CSS BOOTSTRAP -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        a {
            color: white;
            margin-left: 15px;
        }

        a:hover,
        a:active {
            color: #C2F261;
        }
    </style>

    <!-- CDN JAVASCRIPT BOOTSTRAP -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>

<body style="background: #8080808c; font-family: sans-serif;">
    <div class="container-fluid" style="padding-right: 0px; padding-left: 0px;">
        <nav class="navbar" style="background-color: #373737;">
            <nav class="nav">
                <a class="nav-link active" href="index.html">Inicio</a>
                <a class="nav-link" href="galeria.html">Galería</a>
                <a class="nav-link" href="multimedia.html">Video & audio</a>
                <a class="nav-link" href="contacto.php">Contacto</a>
            </nav>
        </nav>
    </div><br><br>

    <h2 style="margin-left:5%;">Contacto</h2><br>
    <div class="container-fluid">
        <div class="container">
            <form name="frmContacto" method="post" action="sendbymail.php">
                <div class="form-group">
                    <label for="nombre">Nombre: </label>
                    <input type="text" class="form-control" id="nombre" name="nombre">
                </div>

                <div class="form-group">
                    <label for="correo">Correo: </label>
                    <input type="email" class="form-control" id="correo" name="correo">
                </div>

                <div class="form-group">
                    <label for="mensaje">Mensaje: </label>
                    <textarea class="form-control" id="mensaje" rows="5" name="mensaje"></textarea>
                </div>
                <button type="submit" class="btn btn-dark btn-sm">Enviar</button>
            </form>

            <div class="row justify-content-center">
                <div class="col-2">
                    <a class="btn btn-dark btn-sm" onclick="history.back();">Regresar</a>
                </div>
            </div>
        </div><br>
    </div>
    </div>
    <br>
</body>

</html>